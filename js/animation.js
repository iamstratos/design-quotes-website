(function($)
{

    var textWidth = $('.content .text').outerWidth(),
        specialLineWidth = $('.h2.special').outerWidth(),
        widthLeft = Math.round(textWidth - specialLineWidth);

    var $slideContentTitles = $('.slide .h2 span, .content .bg-h2 span'),
        $slideCover = $('.white-cover'),

        $sidebar = $('.sidebar'),
        $logo = $('.header .h1'),
        $menuIconLines = $('.menu-icon .line'),

        $playBtn = $('.content .btn'),
        $contentText = $('.content .h2, .content .author div'),
        $specialLine = $('.special-line'),

        $footerPages = $('.footer .pages').children(),
        $footerMetaIcons = $('.footer .meta .icon'),
        $footerMetaNum = $('.footer .meta .num, .header .search input, .header .search .icon'),

        $loadCircle = $('#loader .circle'),

        tl = new TimelineMax()
        tlAll = new TimelineMax()
        tlnewDown = new TimelineMax()
        tloldUp = new TimelineMax()
        tloldDown = new TimelineMax()
        tlnewUp = new TimelineMax()
        tlLoad = new TimelineMax({repeat: -1})
        ;

    NProgress.start();

    function updateStatus()
    {
        var ngStatus = NProgress.status,
            ngClear = ngStatus.toString().replace('0.', ''),
            loadStatus = ngClear.slice(0,2);

        if ( loadStatus.length < 2 )
        {
            loadStatus = loadStatus + '0';
        }

        $('#loader .line').css('height', loadStatus + '%');
        $('#loader .percent').text(loadStatus);
    }

    var interval = setInterval(updateStatus, 5);

    $(window).on('load', function()
    {
        $('#loader .line').css('height', '100%');
        $('#loader .percent').text('100');
        $('#loader').fadeOut(400);

        setTimeout(function()
        {
            NProgress.done();
            clearInterval(interval);
            tl.play();
        }, 500);
    });

    tl
        // .pause()

        // set
        .set($slideContentTitles, {y: '100%'})
        .set($slideCover, {width: '100%'})

        .set($sidebar, {x: -200})
        .set($logo, {y: -200})
        .set($menuIconLines, {y: -200})

        .set($playBtn, {scale: 0})
        .set($contentText, {y: -140, autoAlpha: 0})
        .set($specialLine, {width: 0})

        .set($footerPages, {x: -90, autoAlpha: 0})
        .set($footerMetaIcons, {scale: 0})
        .set($footerMetaNum, {x: -30, autoAlpha: 0})

        .pause()

        // animate
        .add('intro')
        .to($slideCover, .8, {width: '0%', ease:Power2.easeInOut})
        .to($sidebar, 1, {x: 0}, 'intro')
        .to($logo, 1, {y: 0, ease:Power2.easeInOut}, 'intro')
        .staggerTo($menuIconLines, .8, {y: 0, ease:Power1.easeOut}, -0.2, 'intro')
        
        .staggerTo($footerMetaIcons, .4, {scale: 1, ease:Back.easeOut.config(2.3)}, 0.2, 'intro+=.6')
        .staggerTo($footerMetaNum, .3, {x: 0, autoAlpha: 1, ease:Power2.easeInOut}, 0.2, 'intro+=.6')
        .staggerTo($footerPages, .35, {x: 0, autoAlpha: 1, ease:Power1.easeOut}, -0.2, 'intro+=.6')

        .staggerTo($contentText, .6, {y: 0, autoAlpha: 1, ease:Power2.easeInOut}, -0.1, 'intro-=.5')
        .to($playBtn, .5, {scale: 1, ease:Power2.easeInOut}, 'intro+=1.1')
        .to($specialLine, .5, {width: (widthLeft - 90), ease:Power2.easeInOut}, 'intro+=1')

        .to($slideContentTitles, .8, {y: '0%', ease:Power2.easeInOut, onComplete: enable}, 'intro+=1')
        ;

    // Get active content's number and length, display them in page
    var getContent = $('.content.active')
                .attr('class')
                .split(' ')[1]
                .replace('content-', ''),
        contentNum = parseInt(getContent),
        contentLength = $('.content').length;

    // Hide all content except current slide on load
    TweenLite.set( $('.content'), {autoAlpha: 0});
    TweenLite.set( $('.content-' + contentNum), {autoAlpha: 1});
    TweenLite.set( $('.slide'), {autoAlpha: 0});
    TweenLite.set( $('.slide-' + contentNum), {autoAlpha: 1});

    // Add scroll event listeners & scrollBool to true
    function enable() {
        document.addEventListener('mousewheel', function(e)
        {
            animate(e);
        });
        document.addEventListener('DOMMouseScroll', function(e)
        {
            animate(e);
        });
        document.body.addEventListener('keydown', function(e)
        {
            animate(e.keyCode);
        });
    }

    var scrollBool = true;
    function disableScroll()
    {
        scrollBool = false;
    }
    function enableScroll()
    {
        scrollBool = true;
    }

    
    function animate(e)
    {
        // Check if scroll is true
        if (!scrollBool)
        {
            return;
        }

        var scrollData = e.wheelDelta || -e.detail;

        if (e == 38 || e == 33)
        {
            scrollData = 1;
        } else if (e == 40 || e == 34 || e == 32) {
            scrollData = -1;
        }

        // Set active content's number and length
        getContent = $('.content.active')
                .attr('class')
                .split(' ')[1]
                .replace('content-', '');
        contentNum = parseInt(getContent);
        contentLength = $('.content').length;

        if (scrollData > 0 && contentNum > 1)
        {
            disableScroll();

            tloldDown // Current slide goes down
                .add('slideDown')
                .staggerTo( $('.content-' + contentNum + ' .h2, .content-' + contentNum + ' .author div'), .5, {y: '1000%', autoAlpha: 0, ease:Power2.easeInOut}, -0.2)
                .to( $('.content-' + contentNum + ' .btn'), .4, {scale: 0, ease:Power1.easeInOut }, 'slideDown-=.1')
                .to( $('.content-' + contentNum + ' .special-line'), .5, {width: '0%', ease:Power2.easeInOut}, 'slideDown-=.1')
                .to( $('.content-' + contentNum + ' .bg-h2 span'), .9, {y: '100%', ease:Power1.easeInOut}, 'slideDown+=.3')
                .to( $('.slide-' + contentNum + ' .h2 span' ) , .7, {y: '100%', ease:Power1.easeInOut}, 'slideDown')
                .to( $('.slide-' + contentNum ) , .7, {y: '-100%', ease:Power2.easeInOut}, 'slideDown+=.6')
                .staggerTo( $footerPages , .5, {x: 70, autoAlpha: 0, ease:Power1.easeInOut}, -0.1, 'slideDown')
                .set($footerPages, {x: -90, autoAlpha: 0, onStart: setPager, onStartParams:[-1] }, 'slideDown+=.7')
                ;

            tlnewUp // New slide will come up
                // set
                .set( $('.content-' + (contentNum - 1)) , {autoAlpha: 1})
                .set( $('.content-' + (contentNum - 1) + ' .h2, .content-' + (contentNum - 1) + ' .author div') , {y: '-1000%', autoAlpha: 0})
                .set( $('.content-' + (contentNum - 1) + ' .btn') , {scale: 0})
                .set( $('.content-' + (contentNum - 1) + ' .special-line') , {width: 0})
                .set( $('.content-' + (contentNum - 1) + ' .bg-h2 span') , {y: '100%'})
                .set( $('.slide-' + (contentNum - 1) ) , {y: '100%', autoAlpha: 1})
                .set( $('.slide-' + (contentNum - 1) + ' .h2 span' ), {y: '100%', })

                // animate
                .add('newSlideUp')
                .staggerTo( $('.content-' + (contentNum - 1) + ' .h2, .content-' + (contentNum - 1) + ' .author div') , .6, {y: '0%', autoAlpha: 1, ease:Power2.easeInOut}, -0.1, 'newSlideUp+=1.2')
                .to( $('.content-' + (contentNum - 1) + ' .btn') , .5, {scale: 1, ease:Power2.easeInOut}, 'newSlideUp+=1.9')
                .to( $('.content-' + (contentNum - 1) + ' .special-line') , .5, {width: (widthLeft - 90), ease:Power2.easeInOut}, 'newSlideUp+=1.9')
                // .to( $('.content-' + (contentNum - 1) + ' .special-line') , .5, {width: (widthLeft - 90), ease:Power2.easeInOut, onComplete: setActiveContent, onCompleteParams:[1]}, 'newSlideUp+=1.9')
                .to( $('.content-' + (contentNum - 1) + ' .bg-h2 span') , .8, {y: '0%', ease:Power2.easeInOut, onComplete: enableScroll}, 'newSlideUp+=1.6')
                .to ( $('.slide-' + (contentNum - 1) ) , .5, {y: '0%', ease:Power2.easeInOut}, 'newSlideUp+=.6')
                .to ( $('.slide-' + (contentNum - 1) + ' .h2 span' ) , .8, {y: '0%', ease:Power2.easeInOut}, 'newSlideUp+=1.6')
                .staggerTo($footerPages, .35, {x: 0, autoAlpha: 1, ease:Power1.easeOut}, -0.2, 'newSlideUp+=1')
                ;

                setActiveContent(1);
        
        } else if (scrollData < 0 && contentNum < contentLength) {
            disableScroll();

            tloldUp // Current slide goes up
                .add('slideUp')
                .staggerTo( $('.content-' + contentNum + ' .h2, .content-' + contentNum + ' .author div'), .5, {y: '-1000%', autoAlpha: 0, ease:Power2.easeInOut}, 0.2)
                .to( $('.content-' + contentNum + ' .btn'), .4, {scale: 0, ease:Power1.easeInOut}, 'slideUp+=.4')
                .to( $('.content-' + contentNum + ' .special-line'), .5, {width: '0%', ease:Power2.easeInOut}, 'slideUp-=.1')
                .to( $('.content-' + contentNum + ' .bg-h2 span'), .7, {y: '-100%', ease:Power1.easeInOut}, 'slideUp')
                .to( $('.slide-' + contentNum + ' .h2 span' ) , .7, {y: '-100%', ease:Power1.easeInOut}, 'slideUp')
                .to( $('.slide-' + contentNum ) , .8, {y: '100%', ease: Power3.easeIn}, 'slideUp+=.6')
                .staggerTo( $footerPages , .5, {x: 70, autoAlpha: 0, ease:Power1.easeInOut}, -0.1, 'slideUp')
                .set($footerPages, {x: -90, autoAlpha: 0, onStart: setPager, onStartParams:[1] }, 'slideUp+=.7')
                ;

            tlnewDown // New slide will come from below
                // set
                .set( $('.content-' + (contentNum + 1)) , {autoAlpha: 1})
                .set( $('.content-' + (contentNum + 1) + ' .h2, .content-' + (contentNum + 1) + ' .author div') , {y: '1000%', autoAlpha: 0})
                .set( $('.content-' + (contentNum + 1) + ' .btn') , {scale: 0})
                .set( $('.content-' + (contentNum + 1) + ' .special-line') , {width: 0})
                .set( $('.content-' + (contentNum + 1) + ' .bg-h2 span') , {y: '100%'})
                .set( $('.slide-' + (contentNum + 1) ) , {y: '-100%', autoAlpha: 1})
                .set ( $('.slide-' + (contentNum + 1) + ' .h2 span' ), {y: '100%', })

                // animate
                .add('newSlideDown')
                .staggerTo( $('.content-' + (contentNum + 1) + ' .h2, .content-' + (contentNum + 1) + ' .author div') , .6, {y: '0%', autoAlpha: 1, ease:Power2.easeInOut}, 0.1, 'newSlideDown+=1.2')
                .to( $('.content-' + (contentNum + 1) + ' .btn') , .5, {scale: 1, ease:Power2.easeInOut}, 'newSlideDown+=1.9')
                // .to( $('.content-' + (contentNum + 1) + ' .special-line') , .5, {width: (widthLeft - 90), ease:Power2.easeInOut, onComplete: setActiveContent, onCompleteParams:[-1] }, 'newSlideDown+=1.2')
                .to( $('.content-' + (contentNum + 1) + ' .special-line') , .5, {width: (widthLeft - 90), ease:Power2.easeInOut}, 'newSlideDown+=1.2')
                .to( $('.content-' + (contentNum + 1) + ' .bg-h2 span') , .8, {y: '0%', ease:Power2.easeInOut, onComplete: enableScroll}, 'newSlideDown+=1.6')
                .to ( $('.slide-' + (contentNum + 1) ) , .8, {y: '0%', ease: Power3.easeIn}, 'newSlideDown+=.6')
                .to ( $('.slide-' + (contentNum + 1) + ' .h2 span' ) , .8, {y: '0%', ease:Power2.easeInOut}, 'newSlideDown+=1.6')
                .staggerTo($footerPages, .35, {x: 0, autoAlpha: 1, ease:Power1.easeOut}, -0.2, 'newSlideDown+=1')
                ;

                setActiveContent(-1);
        }
    }

    function setActiveContent(num)
    {

        if (num >= 0 && contentNum > 1)
        {
            $('.content').removeClass('active');
            $('.content-' + (contentNum - 1)).addClass('active');
        } else if (num < 0 && contentNum < contentLength) {
            $('.content').removeClass('active');
            $('.content-' + (contentNum + 1)).addClass('active');
        }
        
        getContent = $('.content.active')
                .attr('class')
                .split(' ')[1]
                .replace('content-', ''),
        contentNum = parseInt(getContent),
        contentLength = $('.content').length;

        var $mainLeft = $('.main-left');

        if (contentNum == contentLength)
        {
            $mainLeft.addClass('last');
        } else {
            if ($mainLeft.hasClass('last'))
            {
                $mainLeft.removeClass('last');
            }
        }
    }

    function setPager(num)
    {
        $('.footer .pages .span').eq(0).text(contentNum);
        $('.footer .pages .span').eq(2).text(contentLength);
    }

    setPager(0);

    // extra events

    $('body').on('click', '.nextPage', function(e)
    {
        animate(e);
    });

    $('body').on('click', '.menu-icon', function()
    {
        disableScroll();

        var tlMenuOpen = new TimelineMax(),
            menuList = $('.menu-layer .list').children(),
            info = $('.menu-layer .info').children();

        tlMenuOpen
            .set( $('.menu-layer'), {width: '50%'} )
            .to( $('.menu-layer'), .6, {width: '100%', ease:Power3.easeInOut} );

        tloldDown // Current slide goes down
                .add('slideDown')
                .set( $('.closeMenu'), {y: -300, autoAlpha: 1} )
                .set( menuList, {x: -500} )
                .set( info, {y: -60, autoAlpha: 0} )
                .staggerTo( $('.content-' + contentNum + ' .h2, .content-' + contentNum + ' .author div'), .5, {y: '1000%', autoAlpha: 0, ease:Power3.easeInOut}, -0.1, 'slideDown-=1.8')
                .to( $('.content-' + contentNum + ' .btn'), .3, {scale: 0, ease:Power1.easeInOut }, 'slideDown-=.1')
                .to( $( $sidebar), .3, {x: -200, ease:Power2.easeInOut }, 'slideDown-=.1')
                .to( $('.content-' + contentNum + ' .special-line'), .4, {width: '0%', ease:Power2.easeInOut}, 'slideDown-=.1')
                .to( $('.content-' + contentNum + ' .bg-h2 span'), .5, {y: '100%', ease:Power1.easeInOut}, 'slideDown+=.1')
                .staggerTo( $footerPages , .3, {x: 70, autoAlpha: 0, ease:Power1.easeInOut}, -0.1, 'slideDown-=1.5')
                .staggerTo( $('.footer .meta') , .3, {x: 70, autoAlpha: 0, ease:Power1.easeInOut}, -0.1, 'slideDown-=1.5')
                .to( $('.main-left'), .5, {autoAlpha: 0}, 'slideDown+=.3' )
                .to( $('.menu-layer .line'), .6, {width: '100%', ease:Power3.easeInOut}, 'slideDown+=.5' )
                .to( $('.closeMenu'), 1.4, {y: 0, ease:Power4.easeInOut}, 'slideDown'  )
                .staggerTo( menuList, .5, {x: 0, ease:Power3.easeInOut}, 0.1, 'slideDown+=.2')
                .staggerTo( info, .6, {y: 0, autoAlpha: 1, ease:Back.easeOut.config(2.3)}, 0.3, 'slideDown+=.8')
                ;
    });

    $('body').on('click', '.closeMenu', function()
    {
        var tlMenuClose = new TimelineMax(),
            menuList = $('.menu-layer .list').children(),
            info = $('.menu-layer .info').children();

        tlMenuClose
            .to( $('.menu-layer'), .6, {width: '0%', ease:Power3.easeInOut} );

        tlnewUp // New slide will come up
                // set
                .set( $('.content-' + contentNum) , {autoAlpha: 1})
                .set( $('.content-' + contentNum + ' .h2, .content-' + contentNum + ' .author div') , {y: '-1000%', autoAlpha: 0})
                .set( $('.content-' + contentNum + ' .btn') , {scale: 0})
                .set( $('.content-' + contentNum + ' .special-line') , {width: 0})
                .set( $('.content-' + contentNum + ' .bg-h2 span') , {y: '100%'})
                .set( $('.footer .meta'), {x: 0, autoAlpha: 1})
                .set($footerMetaIcons, {scale: 0})
                .set($footerMetaNum, {x: -30, autoAlpha: 0})

                // animate
                .add('newSlideUp')
                .to( $('.menu-layer .line'), .6, {width: '0%', ease:Power3.easeInOut}, 'newSlideUp' )
                .to( $('.closeMenu'), .3, {autoAlpha: 0, ease:Power3.easeInOut}, 'newSlideUp' )
                .to( $('.main-left'), .5, {autoAlpha: 1}, 'newSlideUp+=.3' )
                .staggerTo( $('.content-' + contentNum + ' .h2, .content-' + contentNum + ' .author div') , .6, {y: '0%', autoAlpha: 1, ease:Power2.easeInOut}, -0.1, 'newSlideUp')
                .to( $('.content-' + contentNum + ' .special-line') , .5, {width: (widthLeft - 90), ease:Power2.easeInOut}, 'newSlideUp+=.4')
                .to( $('.content-' + contentNum + ' .bg-h2 span') , .8, {y: '0%', ease:Power2.easeInOut, onComplete: enableScroll}, 'newSlideUp+=.3')
                .to( $('.content-' + contentNum + ' .btn') , .5, {scale: 1, ease:Power2.easeInOut}, 'newSlideUp+=.2')
                .to( $( $sidebar), .3, {x: 0, ease:Power2.easeInOut }, 'newSlideUp-=.5')
                .staggerTo($footerPages, .35, {x: 0, autoAlpha: 1, ease:Power1.easeOut}, -0.2, 'newSlideUp-=.2')
                // .staggerTo( $('.footer .meta') , .3, {x: 0, autoAlpha: 1, ease:Power1.easeInOut}, -0.1, 'newSlideUp-=1.5')
                .staggerTo($footerMetaIcons, .4, {scale: 1, ease:Back.easeOut.config(2.3)}, 0.2, 'newSlideUp+=.2')
                .staggerTo($footerMetaNum, .3, {x: 0, autoAlpha: 1, ease:Power2.easeInOut}, 0.2, 'newSlideUp+=.2')
                .staggerTo( menuList, .5, {x: -500, ease:Power3.easeInOut}, -0.1, 'newSlideUp-=.1')
                .staggerTo( info, .6, {autoAlpha: 0, ease:Power3.easeInOut}, 0.1, 'newSlideUp-=.2')
                ;
        enableScroll();
    });

    $('.nextPage').hover(function()
    {
        TweenLite.to( $(this), .5, {x: 10} );
    }, function()
    {
        TweenLite.to( $(this), .5, {x: 0} );
    });

    $('.closeMenu').hover(function()
    {
        TweenLite.to( $(this), .45, {rotation: 180, ease:Power3.easeInOut} );
    }, function()
    {
        TweenLite.to( $(this), .3, {rotation: 0} );
    });

    $('.menu-icon').hover(function()
    {
        var tlMenuIcon = new TimelineMax();
        tlMenuIcon
            .staggerTo( $(this).find('.line'), .5, {width: '25%', autoAlpha: 0}, 0.1 )
            .add( TweenLite.to( $(this).find('.line-3'), .2, {width: '100%', autoAlpha: 1}) )
            .add( TweenLite.to( $(this).find('.line-2'), .2, {width: '100%', autoAlpha: 1}) )
            .add( TweenLite.to( $(this).find('.line-1'), .2, {width: '100%', autoAlpha: 1}) );
    });

    
})(jQuery);